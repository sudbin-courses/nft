// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/IERC721Metadata.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract NFT_TOKEN is ERC721, Ownable {
	/**
	 * Это одно из видов хранилища данных (ключей и значений) который называется Storage
	 * Здесь хранятся все токены принадлежащие пользователю.
	 */
	mapping(uint256 => string) public _tokenURIs;

	/**
	 * URL-адрес IPFS папки, содержащей метаданные JSON
	 * 
	 * Предполагается, что метаданные токена 1 будут доступны по адресу
	 * ipfs:/{baseURI}/1,
	 * а метаданные токена 2 будут доступны по адресу
	 * ipfs:/{baseURI}/2 и т.д.
	 */
	string public baseTokenURI;

	constructor() ERC721("NFT_TOKEN", "NFT_1T") {}

    function _baseURI() internal view virtual override returns (string memory) {
        return baseTokenURI;
    }

    function setBaseURI(string memory _baseTokenURI) public onlyOwner {
        baseTokenURI = _baseTokenURI;
    }

	function tokenURI(uint256 tokenId) public view virtual override returns (string memory) {
		require(_exists(tokenId), "ERC721Metadata: URI query for nonexistent token");

		string memory _tokenURI = _tokenURIs[tokenId];
		return _tokenURI;
	}

	function _setTokenURI(uint256 tokenId, string memory _tokenURI) internal virtual {
		require(_exists(tokenId), "ERC721Metadata: URI set of nonexistent token");
		_tokenURIs[tokenId] = _tokenURI;
	}

	function mintToken(
		address _to,
		uint256 _tokenId,
		string memory tokenURI_
	) public payable onlyOwner {
		_safeMint(_to, _tokenId);
		_setTokenURI(_tokenId, tokenURI_);
	}
}
