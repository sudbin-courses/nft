import Moralis from 'moralis'
import "dotenv/config"

async function mint() {
  
    console.log(`\nMinting NFT...`)

    const hh = require('hardhat')
    const ethers = hh.ethers

    const [signer] = await ethers.getSigners()
    const nftContract = require('../deployments/localhost/NFT_TOKEN.json')
    let address = ''
    console.log('Env: ' + process.env.NODE_ENV)
    switch (process.env.NODE_ENV) {
        case 'dev':
            address = '0xDf54829220341b52b601C143089362a4611b9738'
        case 'local':
        default:
            address = nftContract.address
            break;
    }
    const nft = new ethers.Contract(address, nftContract.abi, signer)

    await Moralis.start({
        apiKey: process.env.MORALIS_API_KEY
    });

    const abi = [{
        path: "./assets/icon.png",
        content: require('base64-img').base64Sync("./assets/icon.png"),
    }];

    // 'https://ipfs.moralis.io:2053/ipfs/QmShRRswsyGRG99xTz6QXZvDqnnKgzmJDJ4ydsCuVuoPbJ/./assets/icon.png'
    const path = (await Moralis.EvmApi.ipfs.uploadFolder({ abi })).toJSON()[0].path;
    console.log('Path: ' + path)

    const mintTx = await nft.mintToken(signer.address, '110231023', path)
    console.log(mintTx)

}

mint()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error)
        process.exit(1)
    })
