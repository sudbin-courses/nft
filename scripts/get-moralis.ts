import Moralis from 'moralis'
import { EvmChain } from "@moralisweb3/evm-utils";
import "dotenv/config"

async function mint() {
  
    console.log(`\nGet NFT info by moralis`)

    await Moralis.start({
        apiKey: process.env.MORALIS_API_KEY
    });

    const response = await Moralis.EvmApi.nft.getNFTMetadata({
        address: '0x5fbdb2315678afecb367f032d93f642f64180aa3',
        chain: EvmChain.GOERLI,
        tokenId: '110231023',
    });
  
    console.log(response);
}

mint()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error)
        process.exit(1)
    })
