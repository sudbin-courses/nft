import { HardhatRuntimeEnvironment } from "hardhat/types"
import { DeployFunction } from "hardhat-deploy/types"
import verify from "../utils/verify"

const deployNftToken: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
	// @ts-ignore
	const { getNamedAccounts, deployments } = hre
	const { deploy } = deployments
	const { deployer } = await getNamedAccounts()
	console.log("----------------------------------------------------")
	console.log("Deploying NftToken and waiting for confirmations...")
	const smartContract = await deploy("NFT_TOKEN", {
		from: deployer,
		args: [],
		log: true,
		waitConfirmations: 1,
	})
  console.log(`NftToken at ${smartContract.address}`)
  
  if (process.env.NODE_ENV == 'local') {
    console.log('Verification is not supported for local env')
  } else {
    await verify(smartContract.address, [])
  }
}

export default deployNftToken
deployNftToken.tags = ["all", "nft"]